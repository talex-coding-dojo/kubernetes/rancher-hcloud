# Rancher on Hetzner Cloud

![homer ranger](https://media.giphy.com/media/xT5LMLH7xR9ZnWQQ9y/source.gif)

Git repository for my WueTech October Meetup: [Kubernetes Multi-Cluster Umgebungen mit Rancher 2.6](https://www.meetup.com/de-DE/wue-tech/events/281066611/)

Includes deployment of [Rancher 2 Hetzner Cloud UI Driver](https://github.com/mxschmitt/ui-driver-hetzner)
and [HCloud CSI Driver](https://github.com/hetznercloud/csi-driver) to support provisioning of HCloud volumes directly from the Kubernetes cluster.
Also, [HCloud CCM](https://github.com/hetznercloud/hcloud-cloud-controller-manager) may be deployed to integrate your cluster into HCloud for provisioning of e.g. HCloud Load Balancers.

_Hint: Some deployment example manifests are included in this Git repository, please see below._

## Credits

Based on the work of [Alexander Zimmermann](https://github.com/alexzimmer96) and his [Rancher on Hetzner Cloud](https://github.com/alexzimmer96/rancher-hcloud) GitHub repo.

## TL;DR

Copy `terraform.tfvars.sample` to `terraform.tfvars` and apply your changes.
At least you need to create an access token in Hetzner Cloud and DNS Console and add these tokens as values for the Terraform variables. 
Also, a valid domain (managed by Hetzner DNS Console) is needed for running this setup.

```bash
cd terraform/

terraform init
terraform apply -target=module.rke_create # approve plan with "yes"

export KUBECONFIG=./.kube/config.yaml
kubectl get nodes

terraform apply -target=module.rancher-init # approve plan with "yes"
```

Or even more easy:

```bash
cd terraform/
make all
```

The Rancher Management Plane will be available at https://admin.rancher.domain.org where `domain.org` is your domain as configured in `terraform.tvars`.
Use `admin` as user and your configured password from `terraform.tvars` to login.

## Try the Rancher Admin UI

As soon as the Rancher Management Plane is up and running, start creating a _Developer Cloud_ Kubernetes cluster.

This can be done by using the Rancher Admin UI:

1. Create a Node Template for Hetzner.
2. Copy the `rke-template.yaml.sample` file and add your HCloud token. This is a working Rancher Cluster Template.
3. Create a Cluster Template for Hetzner and provide the content of the YAML file above.
4. Watch your new cluster being created :)

Now, download and store the `KUBECONFIG` of your new cluster and maybe continue with some experiments below.

## Testing

Some YAML manifests have been included into this Git repository, feel free to use them for testing.

### PVC with HCloud CSI Driver

Apply some test deployment of a Persistent Volume Claim (PVC) and watch the results:

```bash
export KUBECONFIG=my-dev-cluster-config.yaml

kubectl apply -f k8s/pvc-test.yaml
kubectl get pvc --watch
```

### LB with HCloud CCM Driver

Apply some test deployment of an Echo Server and try to connect:

```bash
export KUBECONFIG=my-dev-cluster-config.yaml

kubectl apply -f k8s/lb-test.yaml
kubectl get svc --watch

export ECHOSERVER_IPADDR=$(kubectl get svc echoserver -o jsonpath="{.status.loadBalancer.ingress[0].ip}")
curl "http://${ECHOSERVER_IPADDR}"
```

### NATS Message Bus

Deploy a [NATS Message Bus](https://nats.io) using a Kubernetes Operator and its Custom Resource Definition (CRD):

```bash
kubectl apply -f https://github.com/nats-io/nats-operator/releases/latest/download/00-prereqs.yaml
kubectl apply -f https://github.com/nats-io/nats-operator/releases/latest/download/10-deployment.yaml

kubectl get crd | grep nats

cat <<EOF | kubectl create -f -
apiVersion: nats.io/v1alpha2
kind: NatsCluster
metadata:
  name: example-nats-cluster
spec:
  size: 3
  version: "1.3.0"
EOF

kubectl get nats --all-namespaces
```

Connect to your _Developer Cloud_ by using a Kubernetes port-forward:

```bash
kubectl port-forward svc/example-nats-cluster-mgmt 8222
nats-top

kubectl port-forward svc/example-nats-cluster 4222
nats-pub UserCreatedEvent '{ "user_id": 1, "user_name": "Mr. X" }'

kubectl port-forward svc/example-nats-cluster 4222
nats-sub UserCreatedEvent
```

_Hint: See [NATS Go Examples](https://github.com/nats-io/go-nats-examples) and [nats-top](https://github.com/nats-io/nats-top) Git repositories for how to install the NATS tools._

## Documentation

The setup as provided by this Git repository creates resources in the Hetzner Cloud using the following [Terraform](https://www.terraform.io) providers:

### RKE Provider

The RKE provider is used to interact with [Rancher Kubernetes Engine](https://rancher.com/docs/rke/latest/en/) clusters.

Rancher Kubernetes Engine (RKE) is a CNCF-certified Kubernetes distribution that runs entirely within Docker containers.
It works on bare-metal and virtualized servers. RKE solves the problem of installation complexity, a common issue in the Kubernetes community.
With RKE, the installation and operation of Kubernetes is both simplified and easily automated.

> [Documentation for Terraform RKE Provider](https://registry.terraform.io/providers/rancher/rke/latest/docs)

### Hetzner Cloud Provider

The Hetzner Cloud (HCloud) provider is used to interact with the resources supported by [Hetzner Cloud](https://www.hetzner.com/cloud).
The provider needs to be configured with the proper credentials before it can be used.

> [Documentation for Terraform HCloud Provider](https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs)

### Hetzner DNS Provider

This provider helps you automate management of DNS zones and records at [Hetzner DNS Console](https://www.hetzner.com/dns-console).
The provider needs to be configured with the proper credentials before it can be used.

>[Documentation for Terraform Hetzner DNS Provider](https://registry.terraform.io/providers/timohirt/hetznerdns/latest/docs)

### Helm Provider

The Helm provider is used to deploy software packages in Kubernetes using the [Helm](https://helm.sh/) package manager for Kubernetes.
The provider needs to be configured with the proper credentials before it can be used.

> [Documentation for Terraform Helm Provider](https://registry.terraform.io/providers/hashicorp/helm/latest/docs)

### Rancher2 Provider

The Rancher2 provider is used to interact with the resources supported by [Rancher v2](https://rancher.com/docs/rancher/v2.6/en/).
The provider can be configured in two modes, admin and bootstrap. This setup makes use of both modes.

> [Documentation for Terraform Rancher2 Provider](https://registry.terraform.io/providers/rancher/rancher2/latest/docs)
