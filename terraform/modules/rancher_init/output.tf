output "rancher_admin_token" {
  value = rancher2_bootstrap.setup_admin.token
}

output "hetzner_driver_id" {
  value = rancher2_node_driver.hetzner_node_driver.id
}
