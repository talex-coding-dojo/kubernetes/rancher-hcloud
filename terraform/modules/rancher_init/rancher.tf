resource "helm_release" "rancher" {
  name       = "rancher"
  namespace  = "cattle-system"
  chart      = "rancher"
  repository = "https://releases.rancher.com/server-charts/latest"
  depends_on = [helm_release.cert_manager]

  wait             = true
  create_namespace = true
  force_update     = true
  replace          = true

  timeout = 600

  set {
    name  = "hostname"
    value = "admin.rancher.${var.project_domain}"
  }

  set {
    name  = "ingress.tls.source"
    value = "letsEncrypt"
  }

  set {
    name  = "bootstrapPassword"
    value = var.rancher_admin_password
  }
}

resource "rancher2_bootstrap" "setup_admin" {
  #provider = rancher2.bootstrap

  password         = var.rancher_admin_password
  current_password = var.rancher_admin_password

  telemetry  = false
  depends_on = [helm_release.rancher]
}

